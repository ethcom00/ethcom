// SPDX-License-Identifier: MIT
import "./Landable.sol";
pragma solidity >=0.4.21 <0.9.0;


contract Recipiant is Landable(){
  
  address _domainHashAdd;
  uint postCounter;
  bool _freePost;



  constructor(string memory first_post, address domainHashAdd , address firstComContract, uint firstComIndex , address eveeContract){
    _tokenId = firstComIndex;
    _NFTContract = firstComContract;
    _freePost = true;
    whiteListEvee(eveeContract);
    changedomainHashAdd (domainHashAdd);
    post(first_post,0);
  }
  
  event post_com(uint indexed id, address indexed NFTContract , uint indexed tokenId, bool freePost);
  event post_msg(uint indexed id, uint indexed prev ,string body, address indexed sender);
  //commercialTXTest - delete
  event comTest(string testttt);
  function comSet(string memory y) public {
    emit comTest(y);
  }
  //end/commercialTXTest - delete


  function post(string memory input, uint prev) public {
    require (prev < postCounter || prev == 0, 'commenting on non-existing post');
    if (_sender == address(0)) {
      _sender = msg.sender;
    }
    emit post_com(postCounter, _NFTContract, _tokenId, _freePost);
    emit post_msg(postCounter,prev, input, _sender);
    _tokenId = 0;
    _NFTContract = address(0);
    _freePost = false;
    _sender = address(0);
    postCounter ++;
  } 


  function land (bytes memory senderTxData, bytes memory comTxData, uint tokenId ,address NFTContract, address sender, address comSender) override public isWhiteListedEvee() {
    _freePost = true;
    //commercialTXTest - replace with super.land(senderTxData,tokenId ,NFTContract, sender);
    super.land(senderTxData,comTxData,tokenId ,NFTContract, sender, comSender);
    //end/commercialTXTest - replace
  } 
  function land (bytes memory senderTxData, uint tokenId ,address NFTContract, address sender) override public isWhiteListedEvee() {
    _freePost = true;
    super.land(senderTxData,tokenId ,NFTContract, sender);
  } 
  
  function encodeEip712DomainHash() public override returns (bytes32){
      require (_domainHashAdd!= address(0), "domainHashAdd was not initated");
      (bool success, bytes memory hash) = _domainHashAdd.delegatecall(
              abi.encodeWithSignature(string('delEncodeEip712DomainHash(address)'),address(this)));
      require(success, string(abi.encodePacked("Failed to delegatecall encodeEip712DomainHash")));
      return bytes32(hash);
   
  
  }
  function changedomainHashAdd (address domainHashAdd) public onlyOwner(){
      _domainHashAdd = domainHashAdd;
  }

}
